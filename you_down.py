#!/usr/bin/python

import pafy
import os 
import sys
import urllib 
import hashlib
import datetime
from dateutil import parser
import time 
from email import utils as email_utils 
from feedgen.feed import FeedGenerator
import configuration as cfg
import xml.etree.ElementTree as ET
from tagger import *

debug = cfg.debug

class Loader:

	def __init__(self, url, frmt):
		"""
		url = url of the video 
		frmt = download format wither audio or video, default = audio
		"""
		if debug : print " this is the url received to the loader function : <", url, ">"
		self.url = url
		self.title = None
		self.author = None
		self.filepath = None
		self.videoid = None
		self.img_url = None
		self.filename = None
		self.filesize = None
		self.pubdate = None
		self.videoPubDate = None
		self.description = None
		self.frmt = frmt
		if frmt == "video":
			cfg.podcast = cfg.podcast_video

	def get_file(self):
		if self.frmt == "video":
			self.get_mp4()
		else:
			self.get_m4a()

	def get_mp4(self):
		video = pafy.new(self.url)
		self.set_data_fields(video)
		best = video.getbest()
		if debug: print "### (get_mp4) got the mp4 stream available (VIDEO) "
		self.filename = cfg.podcast['file_prefix'] + self.videoid + "." + best.extension
		self.filepath = cfg.video_filepath + self.filename
		self.filesize = best.get_filesize()
		# self.description = "VIDEO\nchannel: " + self.author + "\n" + "filesize: " + str(self.filesize) + "\n" + "Video pubdate: " + str(self.videoPubDate) + "\n"
		if debug: print "### file size of video: " + str(self.filesize)
		if self.compile_rss_feed('video'):
			if debug : print "### (get_mp4) downloading episode because it doesn't exist : " + self.filepath
			best.download(filepath=self.filepath, quiet=True)


	def get_m4a(self):
		video = pafy.new(self.url)
		self.set_data_fields(video)
		# self.description = "channel: " + self.author + "\n"
		if video.m4astreams[0]:
			a = video.m4astreams[0]
			if debug: print "### (get_m4a) got the m4a stream available "
			self.filename = cfg.podcast['file_prefix'] + self.videoid + "." + a.extension
			self.filepath = cfg.audio_filepath + self.filename
			self.filesize = a.get_filesize()
			if debug: print "### file size : " + str(self.filesize)
			if self.compile_rss_feed('audio'):
				if debug : print "### (get_m4a) downloading episode because it doesn't exist : " + self.filepath
				a.download(filepath=self.filepath, quiet=True, remux_audio=True)


	def set_data_fields(self, video):
		if debug: print "### (set_data_fields) setting data fields for this url : " + self.url
		self.title = video.title
		self.author = video.author
		self.description = video.description
		self.videoid = video.videoid
		if(video.bigthumbhd):
			self.img_url = video.bigthumbhd
		elif(video.bigthumb):
			self.img_url = video.bigthumb
		else:
			self.img_url = video.thumb
		self.videoPubDate = parser.parse(video.published)
		self.pubdate = email_utils.formatdate(time.time())

	def compile_rss_feed(self, feedtype):
		""" 
		This will create an entry in the podcast rss feed with the current downloaded episode
		"""
		if feedtype == 'audio':
			rss_file_path = cfg.audio_filepath
			f_url = cfg.http_url_audio 
		elif feedtype == 'video':
			rss_file_path = cfg.video_filepath
			f_url = cfg.http_url_video			

		if not self.feed_exists(rss_file_path + cfg.podcast['rss_file']):
			self.create_podcast_xml(feedtype, f_url)
		
		ET.register_namespace('itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd')
		ET.register_namespace('content', 'http://purl.org/rss/1.0/modules/content/')
		ET.register_namespace('atom', 'http://www.w3.org/2005/Atom')
		
		tree = ET.parse(rss_file_path + cfg.podcast['rss_file'])
		root = tree.getroot()
		channel = root.findall('channel')[0]
		
		self.update_last_build_date(channel)

		if self.episode_exists(channel, f_url):
			return False
		if debug: print "### (compile_rss_feed) adding episode to the rss feed"
		item = ET.Element('item')
		ET.SubElement(item, 'title').text = self.title
		episode_img = ET.SubElement(item, '{http://www.itunes.com/dtds/podcast-1.0.dtd}image')
		episode_img.set('href', self.img_url)
		ET.SubElement(item, 'author').text = self.author
		ET.SubElement(item, 'description').text = self.description
		ET.SubElement(item, 'link').text = f_url + self.filename
		ET.SubElement(item, 'pubDate').text = self.pubdate

		
		guid = ET.SubElement(item,'guid')
		guid.set('isPermaLink', 'true')
		guid.text = f_url + self.filename
		
		enclosure = ET.SubElement(item, 'enclosure')
		enclosure.set('url', f_url + self.filename)
		enclosure.set('type', 'audio/mpeg')
		enclosure.set('length', str(self.filesize))

		channel.append(item)
		tree.write(rss_file_path + cfg.podcast['rss_file'], xml_declaration=True)
		return True 

	def update_last_build_date(self, channel):
		last_build = channel.findall('lastBuildDate')[0]
		last_build.text = email_utils.formatdate()


	def feed_exists(self, file):
		"""
			Will check if the feed file exists in order not to overwrite it but read exissting one.
		"""
		if os.path.isfile(file):
			if debug : print("### podcast.xml file exists, no need to generate new", file)
			return True
		else:
			if debug : print("### xml file does not exist, will create file: ", file)
			return False		
	
	def episode_exists(self, channel,f_url):
		"""
			This will check if the episode already exists and if so will not download it by returning True
		"""
		link = f_url + self.filename
		for item in channel.iter('item'):
			if item.find('link').text == link:
				if debug : print "### (episode_exists) this episode already exists " + link
				return True
		if debug : print "### (episode_exists) New episode " + link
		return False

	def create_podcast_xml(self, feedtype, f_url):
		""" 
		This will generate the podcast.xml file for the first time if it didn't exist
		"""
		if debug : print("### (create_podcast_xml) generating feed file ")
		if feedtype == 'audio':
			rss_file_path = cfg.audio_filepath + cfg.podcast['rss_file']
		elif feedtype == 'video':
			rss_file_path = cfg.video_filepath + cfg.podcast['rss_file']

		fg = FeedGenerator()
		fg.load_extension('podcast', atom=False, rss=True)
		fg.id(cfg.podcast['url'])
		fg.description(cfg.podcast['description'])
		fg.title(cfg.podcast['title'])
		fg.link(href=cfg.podcast['url'], rel='self')
 		fg.logo(f_url + cfg.podcast['image'])
		if debug: print(fg.rss_str())
		fg.rss_file(rss_file_path)




