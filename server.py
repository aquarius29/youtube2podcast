#!/usr/bin/python

# from you_down import Loader

# import SimpleHTTPServer
import BaseHTTPServer
import SocketServer
from urlparse import urlparse, parse_qs
from you_down import Loader as loader

PORT = 8081
HOST = ""
# Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
		
	def do_HEAD(self):
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		self.end_headers()
	
	def do_GET(self):
		query_components = parse_qs(urlparse(self.path).query)
		if "vid_url" in query_components:
			vid_url= query_components["vid_url"]			
		if "format" in query_components:
			vid_frmt = query_components["format"]
		else: 
			vid_frmt = "audio"

                self.send_response(200, 'downloading file from ' + str(vid_url) + ' of format ' + str(vid_frmt))
		ldr = loader(vid_url[0], vid_frmt[0])
		ldr.get_file()
		#self.send_response(200, 'downloading file from ' + str(vid_url) + ' of format ' + str(vid_frmt))

	# def do_POST(self):
 #        # Parse the form data posted
	# 	print self.headers['Content-Type']

httpd = SocketServer.TCPServer((HOST, PORT), MyHandler)

print "serving at port", PORT
httpd.serve_forever()

