#!/usr/bin/python

# import pafy
import os 
import sys
import urllib 
# import hashlib
import datetime
from dateutil import parser
import time 
from email import utils as email_utils 
from feedgen.feed import FeedGenerator
# import configuration as cfg
# import xml.etree.ElementTree as ET
from tagger import *

debug = True

class BookCaster:
	"""
		contains funcationality to convert audio files in a folder into rss feed
	"""
	def __init__(self, name, author="author", title="Title", description="description"):
		self.url_base = 'http://betelgeuse.duckdns.org/podcast/media/books/'
		self.path_base = '/var/www/mentorship/podcast/media/books/'

		#name = folder name of the book
		self.book_name = name
		self.url_path = self.url_base + self.book_name 
		self.file_path = self.path_base + self.book_name
		self.author = author
		self.title = title
		self.description = description
		self.image = None

	def create_bookcast(self):
		fg = self.make_rss_file()

		files = self.get_file_list()
		counter = 1000
		for file in files:
			counter -= 20 
			mp3_file_path = self.file_path + "/" + file
			mp3_url_path = self.url_path + "/" + urllib.quote(file)
			mp3_tag = ID3v2(mp3_file_path)
			file_data = self.parse_ID3_frames(mp3_tag, file)
			fe = fg.add_entry()
			fe.id(mp3_url_path)
			fe.link({'href':mp3_url_path})
			fe.title(file_data['title'])
			fe.author(name=file_data['author'], email="")
			if(isinstance(file,unicode)):
				fe.description(file)
			else:
				fe.description('description')
			fe.pubdate(email_utils.formatdate(time.time()+counter)) 
			fe.enclosure(mp3_url_path, length=str(os.path.getsize(mp3_file_path)), type='audio/mpeg')

 		fg.logo(self.image)
		fg.rss_str(pretty=True)
		fg.pubDate(email_utils.formatdate(time.time()-600))		
		fg.rss_file(self.file_path + "/" + "book.xml")



	def parse_ID3_frames(self, tag, filename):
		file_data = {}
		frames = tag.frames
		fids = [frame.fid for frame in frames]
		for fid in fids :
			if fid == "TIT2":
				try:
					# file_data['title'] = str(filename).decode('UTF-8')
					file_data['title'] = frames[fids.index(fid)].strings[0]
				except UnicodeDecodeError:
					print('\n unicodeDecodeError : TIT2')
					file_data['title'] = "Title"
			if fid == "TPE1":
				try:
					file_data['author'] = frames[fids.index(fid)].strings[0]
				except UnicodeDecodeError:
					print('\n unicodeDecodeError : TPE1')
					file_data['author'] = str(self.author)

			if fid == "TALB":
				try:
					file_data['album'] = frames[fids.index(fid)].strings[0]
				except UnicodeDecodeError:
					print('\n unicodeDecodeError : TALB')
					pass
			if fid == "TPE2":
				try:
					file_data['band'] = frames[fids.index(fid)].strings[0]
				except UnicodeDecodeError:
					print('\n unicodeDecodeError : TPE2')
					pass					
			if fid == "TPUB":
				try:
					file_data['publisher'] = frames[fids.index(fid)].strings[0]
				except UnicodeDecodeError:
					print('\n unicodeDecodeError : TPUB')
					pass
			if fid == "APIC":
				self.save_image_from_tag(frames[fids.index(fid)])
		if "TIT2" not in fids:
			try:
				file_data['title'] = str(filename)
			except UnicodeDecodeError:
				print('\n unicodeDecodeError : TIT2, filename')
				file_data['title'] = "Title"
		if "TPE1" not in fids:
			try:
				file_data['author'] = str(self.author)
			except UnicodeDecodeError:
				pass
		if debug : 
			print(">>> this is the tags from file which will be inserted into the podcast entries")
			print "filename: ", filename, "\n"
			print "filedata: \n", file_data
			print "\n ---------- <<<\n"

		return file_data

	def save_image_from_tag(self, frame):
		raw_img = frame.pict
		with open(self.file_path + "/" + self.book_name + ".jpg", 'wb') as img:
			img.write(raw_img)
		self.image = self.url_path + "/" + self.book_name + ".jpg"

	def get_file_list(self):
		file_extensions = ['.mp3', '.m4b']
		lst_all_files = os.listdir(self.file_path)
		lst_all_files.sort()
		lst_audio_files = []
		for file in lst_all_files:
			if any(ext in file for ext in file_extensions):
				lst_audio_files.append(file)
		return lst_audio_files

	def make_rss_file(self):
		""" 
		This will generate the podcast.xml file for the first time if it didn't exist
		"""

		if debug : print("### (create_podcast_xml) generating feed file ")

		fg = FeedGenerator()
		fg.load_extension('podcast', atom=False, rss=True)
		fg.id(self.url_path)
		fg.description(self.description)
		fg.title(self.title)
		fg.author({'name':self.author})
		fg.link(href = self.url_path, rel='self')
		if debug: print(fg.rss_str())
		return fg 

	def is_valid_xml_char_ordinal(self, i):
	    """
	    Defines whether char is valid to use in xml document
	    XML standard defines a valid char as::
	    Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
	    """
	    return ( # conditions ordered by presumed frequency
	        0x20 <= i <= 0xD7FF
	        or i in (0x9, 0xA, 0xD)
	        or 0xE000 <= i <= 0xFFFD
	        or 0x10000 <= i <= 0x10FFFF
	        )


	def clean_xml_string(self, s):
	    """
	    Cleans string from invalid xml chars
	    Solution was found there::
	    http://stackoverflow.com/questions/8733233/filtering-out-certain-bytes-in-python
	    """
	    return ''.join(c for c in s if self.is_valid_xml_char_ordinal(ord(c)))

		
