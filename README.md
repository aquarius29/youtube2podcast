# The project idea #

the idea for this project came from the need to be able to convert youtube videos into audio format, so when running I don't need to have screen unlocked to listen to the lectures or other useful information.

## How it works ##
or rather how *should be working*

from user perspective (this was intended to be used by me only, so there are some shortcuts): 

There is a page where user navigates and sees input text field where a youtube link to a video is placed. Button is clicked and magic happens. Once the button is clicked no more interaction is needed from the user. The converted video will show up in the user's podcast feed. Of course the user should be subscribed to the RSS podcast feed which is taken from the location where the script is running. 

from the technical perspective:

There is a python webserver running on the machine which is listening to post requests. The page described above is posting to that webserver the link to the video to be converted. Once Webserver gets the link it is passing it to the python script which is converting the video to audio (extracting audio) using pafy. It is also possible to get video podcast (basically to watch something from youtube offline). Once the audio (or video) is extracted the file is placed in the media folder on the server. Then and entry to the RSS feed is added which points to that file and adds some metadata about the audio(video) file. Supereasy. 

There is also a manual bookcaster script which is basically podcastifies an audiobook. The idea is that a person places an audiobook in its own folder inside the audiobook directory and then script is creating a podcast entry from that book. It is supposed to make it easy to share audiobooks within family and friends, so they just need to subscribe to an RSS feed with the books and they will show up automatically in their feed. 


## TBD: ##
TBD: make RSS link on the page where videos are inputed.  
TBD: Automate the audiobooks thing. Make the script search yandex disk directory for audiobooks so we don't need to store large files on limited space of our vps.