#!/usr/bin/env python

debug = False
audio_filepath = '/var/www/mentorship/podcast/media/'
http_url_audio = 'http://betelgeuse.duckdns.org/podcast/media/'

video_filepath = '/var/www/mentorship/podcast/media/video/'
http_url_video = 'http://betelgeuse.duckdns.org/podcast/media/video/'

podcast = {
  'title' : "Youtube audio podcast",
  'author': "Youtube contributors",
  'description': "This podcast contains audio from different youtube channels which I would like to listen to while running or doing other stuff",
  'url' : "http://betelgeuse.duckdns.org/podcast/media/",
  'rss_file' : 'podcast.xml',
  'image': 'img/you2audio.png',
  'file_prefix': 'audio_'
}

podcast_video = {
  'title' : "Youtube video podcast",
  'author': "Youtube contributors",
  'description': "This podcast contains video from different youtube channels which I would like to see offline",
  'url' : "http://betelgeuse.duckdns.org/podcast/media/video/",
  'rss_file' : 'podcast.xml',
  'image': 'img/you2video.png',
  'file_prefix': 'video_'
}
